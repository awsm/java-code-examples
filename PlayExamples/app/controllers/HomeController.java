package controllers;

import com.google.inject.Inject;
import models.*;
import org.hibernate.Session;
import play.data.DynamicForm;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    @Inject FormFactory formFactory;


    public Result forms() {

        return ok(forms.render());
    }


    public Result formReceiver() {


        Form<SomeGuy> frodoForm = formFactory.form(SomeGuy.class);
        System.out.println(frodoForm.bindFromRequest().toString());
        SomeGuy frodo = frodoForm.bindFromRequest().get();
        System.out.println("FrodoName: "+frodo.getName());
        System.out.println("FrodoEMail: "+frodo.getEmail());
        System.out.println("FrodoPassWord: "+frodo.getPassword());
        System.out.println("FrodoAge: "+frodo.getAge());
        System.out.println("FrodoSize: "+frodo.getSize());

        return ok(formreceiver.render("FormReceiver!"));
    }



    public Result index() {

        /****************************************************************************/
        // LAZY FETCHING TESTING
        /****************************************************************************/

        Session session = DAO.openSession();
        Customer customer = (Customer) session.createQuery("FROM Customer as c WHERE c.id = 4").uniqueResult();
        // OneToOne Lazy Load
        // Lösung mit dao.getUser() dao.getUserWithMailAccounts() dao.getUserWIthMailAccountsAndHistoricalIds() nehmen -.-
        System.out.println("----------------- DOING NOTHING ---------------------------------");
//        customer.getAddress().getStreet();
        System.out.println("----------------- DOING NOTHING ---------------------------------");
        System.out.println("SHOP ORDERS: ");

        session.close();
//        for(ShopOrder shopOrder : customer.getShopOrders()) {
//            System.out.println("Order: "+shopOrder.getProduct());
//        }


        /****************************************************************************/
        // INSERT TESTING
        /****************************************************************************/

//        Customer customer = new Customer();
//        customer.setFirstName("Franz");
//        customer.setLastName("Ferdinant8");
//
//        Address address = new Address();
//        address.setStreet("Blumenweg. 26");
//        address.setCustomer(customer);
//
//        ShopOrder shopOrder1 = new ShopOrder();
//        shopOrder1.setProduct("Kaffee2");
//        shopOrder1.setCustomer(customer);
//        ShopOrder shopOrder2 = new ShopOrder();
//        shopOrder2.setProduct("Kaffee3");
//        shopOrder2.setCustomer(customer);
//
//        Set<ShopOrder> shopOrderSet = new HashSet<>();
//        shopOrderSet.add(shopOrder1);
//        shopOrderSet.add(shopOrder2);
//
//        customer.setAddress(address);
//        customer.setShopOrders(shopOrderSet);
//
//
//        Session session = DAO.openSession();
//        session.beginTransaction();
//        session.save(customer);
//        session.getTransaction().commit();
//        session.close();



        DynamicForm dynamicForm = formFactory.form();

        return ok(index.render("Your new application is ready."));
    }

}
