package models;


import play.data.validation.Constraints;

/**
 * Created by cc on 17.03.16.
 */
public class SomeGuy {

    private String name;
    private String email;
    private String password;
    private int age;
    private double size;



//    public void setAge(String age) {
//        System.out.println("Trying to make Integer");
//        try {
//
//            this.age = Integer.valueOf(age);
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Age was no int");
//            this.age = 2;
//        }
//
////        if(age instanceof Integer) {
////            System.out.println("Age is an Integer");
////            this.age = (Integer) age;
////        } else {
////            System.out.println("Age is NOT an Integer");
////            this.age = 0;
////        }
//    }

    public void setAge(Integer age) {
        this.age = age;
    }


    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
