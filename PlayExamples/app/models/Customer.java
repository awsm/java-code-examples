package models;



import javax.persistence.*;
import java.util.Set;


@Entity
@Table(
        indexes = {
            @Index(columnList = "firstName, lastName"),
            @Index(columnList = "firstName")
        }
)
public class Customer {

    public Customer() {}



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String firstName;
    
    @Column(nullable = false)
    private String lastName;

    @Column(unique = true)
    private String uniqueString;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Address address;

    @Column
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ShopOrder> shopOrders;

    @Column
//    private


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<ShopOrder> getShopOrders() {
        return shopOrders;
    }

    public void setShopOrders(Set<ShopOrder> shopOrders) {
        this.shopOrders = shopOrders;
    }

    @Override
    public String toString() {
        return "Customer: "+getFirstName()+" "+getLastName()+" - Address: "+getAddress().getStreet();
    }
}
