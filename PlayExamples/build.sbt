name := """PlayExamples"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.hibernate" % "hibernate-core" % "5.1.0.Final",
  javaJdbc,
  cache,
  javaWs
)
libraryDependencies += "org.hibernate.javax.persistence" % "hibernate-jpa-2.1-api" % "1.0.0.Final"
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.38"
