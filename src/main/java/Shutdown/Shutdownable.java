/**
 * Created by cc on 03.08.16.
 */
public interface Shutdownable {

    void shutdown();

}
