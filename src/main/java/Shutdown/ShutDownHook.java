import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShutDownHook implements Runnable {

    final private Logger logger = LoggerFactory.getLogger(ShutDownHook.class);
    final private Shutdownable shutdownable;
    final private String identifierForLogging;

    public ShutDownHook(String identifierForLogging, Shutdownable shutdownable) {
        this.shutdownable = shutdownable;
        this.identifierForLogging = identifierForLogging;
    }

    public void run() {
        this.logger.info("Initializing shut down for: "+this.identifierForLogging);
        this.shutdownable.shutdown();
        this.logger.info("Shutdown complete for: "+this.identifierForLogging);
    }
}
