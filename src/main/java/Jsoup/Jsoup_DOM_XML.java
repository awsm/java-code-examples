import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Jsoup_DOM_XML {

    public static void main(String[] args) {

        /*******************************************************************************/
        // PARSE XML FROM URL
        /*******************************************************************************/
        try {

            Parser parser = Parser.xmlParser();
            Document doc = Jsoup
                    .connect("http://sitemaps.trustpilot.com/index_en-us.xml")
                    .parser(parser)
                    .get();

            // WITH ENCODING
            // Document doc = Jsoup.parse(new URL(line).openStream(), "UTF-8", "");

            for(Element loc : doc.select("loc")) {
                System.out.println(loc.text());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
