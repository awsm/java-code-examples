import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by cc on 10.11.15.
 */
public class ApacheCommonsIO {

    public static void main(String[] args) {

        /*******************************************************************************/
        // READ FILE LINE BY LINE
        /*******************************************************************************/
        try {
            for(String line : FileUtils.readLines(new File("FILENAME"))) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*******************************************************************************/
        // APPEND STRING TO FILE
        /*******************************************************************************/
        try {
            File fileHandle = new File("FILENAME");
            FileUtils.writeStringToFile(fileHandle, "SOME EXAMPLE TEXT WITH NEWLINE\n", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
