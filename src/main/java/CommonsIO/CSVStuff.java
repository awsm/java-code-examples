import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.opencsv.CSVReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Created by cc on 10.11.15.
 */
public class CSVStuff {

    public static void main(String[] args) {

        /******************************************************************************************/
        // JAVACSV - net.sourceforge.javacsv
        /******************************************************************************************/

        /*************************************************/
        // READ
        /*************************************************/
//        try {
//
//            CsvReader products = new CsvReader("VanBL.csv");
//
//            products.readHeaders();
//
//            while (products.readRecord())
//            {
//                String id = products.get("ID Vanessa");
//                String domain = products.get("URL");
//                String status = products.get("status");
//
//                // perform program logic here
//                System.out.println(id +" - "+ domain +" - "+ status);
//            }
//
//            products.close();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        /*************************************************/
        // APPEND
        /*************************************************/

        String outputFile = "users.csv";

        // before we open the file check to see if it already exists
        boolean alreadyExists = new File(outputFile).exists();

        try {
            // use FileWriter constructor that specifies open for appending
            CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');

            // if the file didn't already exist then we need to write out the header line
            if (!alreadyExists)
            {
                csvOutput.write("id");
                csvOutput.write("name");
                csvOutput.endRecord();
            }
            // else assume that the file already has the correct header line

            // write out a few records
            csvOutput.write("1");
            csvOutput.write("Bruce asdf, asdf");
            csvOutput.endRecord();

            csvOutput.write("2");
            csvOutput.write("John asdf asdf");
            csvOutput.endRecord();

            csvOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }






        // ----------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------
        // ----------------------------------------------------------------------------------------


//        try {
//            Reader in = new FileReader("test.csv");
//            Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
//            for (CSVRecord record : records) {
//                String url = record.get(0);
//                String progress = record.get(1).replace("undone", "done");
//                System.out.println(url +" ---- "+progress);
//            }
//            FileWriter fileWriter = new FileWriter("test.csv");
//            CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(",");
//            CSVPrinter csvPrinter = new CSVPrinter(fileWriter, csvFileFormat);
//            csvPrinter.


            /*************************************************/
            // JAVA.IO
            /*************************************************/
//            CSVReader reader = new CSVReader(new FileReader("test.csv"));
//            String [] nextLine;
//            while ((nextLine = reader.readNext()) != null) {
//                // nextLine[] is an array of values from the line
//                System.out.println(nextLine[0] +" ---- "+ nextLine[1]);
//                nextLine[1].replace("undone", "done");
//            }


            /*************************************************/
            // COMMONS
            /*************************************************/
//            CSVParser parser = null;
//            try {
//                File file = new File("bla");
//                parser = CSVParser.parse(file, StandardCharsets.UTF_8, CSVFormat.newFormat('|')); // delimiter = |
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            for (CSVRecord record : parser) {
//                record.get(1); // Get column 1
//                record.get(2); // Get column 2
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

}
