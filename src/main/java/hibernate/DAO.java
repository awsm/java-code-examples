package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Created by cc on 10.11.15.
 */
public class DAO {

    private SessionFactory sessionFactory;


    public DAO() {
        createSessionFactory("");
    }
    public DAO(String startup) {
        createSessionFactory(startup);
    }


    public void createSessionFactory(String startup) {
        ServiceRegistry serviceRegistry;
        SessionFactory sessionFactory;

        Configuration conf = new Configuration();

        conf.configure("hibernate.cfg.xml");

//            new SchemaExport(conf).execute(true, true, false, true);



        System.out.println("#### BUILDING SUM SHIT ######");
        serviceRegistry = new ServiceRegistryBuilder().applySettings(conf.getProperties()).buildServiceRegistry();
        try {
            sessionFactory = conf.buildSessionFactory(serviceRegistry);
        } catch (Exception e) {
            System.err.println("Initial SessionFactory creation failed." + e);
            throw new ExceptionInInitializerError(e);
        }


        this.sessionFactory = sessionFactory;
    }


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }



/*
	public static Session getSession(SessionFactory sessionFactory) {
		Session session;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException ex) {
			session = sessionFactory.openSession();
		}
		return session;
	}
*/

}
