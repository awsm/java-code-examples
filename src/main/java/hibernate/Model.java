package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;


@Table(appliesTo = "Hibernatemodel")
public class Model {
//
//    /*************************************************************/
//    // FIELDS
//    /*************************************************************/
//    @Id
//    @GeneratedValue
//    @Column()
//    private Integer id;
//
//    @Index(name = "domain")
//    @Column(nullable=false)
//    private String domain;
//
//    @Column()
//    private boolean alexa_done = false;
//
//    @Index(name = "export_date")
//    @Column()
//    private Date export_date;
//
//    @Transient
//    @OneToOne(mappedBy = "task_ref", orphanRemoval=true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    private String sagepay;
//
//
//    /*************************************************************/
//    // CONSTRUCTORS
//    /*************************************************************/
//    public Model(){
//    }
//
//
//    /*************************************************************/
//    // CRUD
//    /*************************************************************/
//
//    public static List<String> loadTasksForXoviScraper(SessionFactory sessionFactory) {
//        Session session = sessionFactory.openSession();
//
//        List<String> taskList = (List<String>) session.createQuery("from Task where xovi_done = 0 AND (order_nr = 3 OR order_nr = 4 OR order_nr = 5)").list();
//        System.out.println("Xovi Queue:" + taskList.size());
//        session.clear();
//        session.close();
//
//        return taskList;
//    }
//
//
//    public static void saveBatchNonUnique(SessionFactory sessionFactory, List<Task> urlList) {
//        Session session = sessionFactory.openSession();
//        session.beginTransaction();
//        int i = 0;
//        int countSucces = 0;
//        int countDuplicate = 0;
//        for(Task task : urlList) {
//            session.save(task);
//            countSucces++;
//            if(++i % 1000 == 0) {
//
//                session.flush();
//                session.clear();
//                System.out.println("Saving..."+countSucces);
//            }
//        }
//        System.out.println("Import List Size: "+urlList.size()+" - Duplicate Count: "+countDuplicate+" - Success Count: "+countSucces);
//        session.getTransaction().commit();
//        session.clear();
//        session.close();
//    }
//
//
//
//    public static void updateBatch(SessionFactory sessionFactory, List<Task> urlList) {
//        Session session = sessionFactory.openSession();
//        session.beginTransaction();
//        int i = 0;
//        for(Task url : urlList) {
//            session.update(url);
//            if(++i % 100 == 0) {
//
//                session.flush();
//                session.clear();
//                System.out.println("Updating...");
//            }
//        }
//        session.getTransaction().commit();
//        session.clear();
//        session.close();
//    }
//
//
//    public static Task getTaskById(SessionFactory sessionFactory, int id) {
//        Session session = sessionFactory.openSession();
//
//        Task task = (Task) session.createQuery("from Task where id = :id").setParameter("id", id).uniqueResult();
//
//        session.clear();
//        session.close();
//
//        return task;
//    }
//
//
//    /*************************************************************/
//    // GETTER & SETTER
//    /*************************************************************/
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getDomain() {
//        return domain;
//    }
//
//    public void setDomain(String domain) {
//        this.domain = domain;
//    }
//
//    public boolean isAlexa_done() {
//        return alexa_done;
//    }
//
//    public void setAlexa_done(boolean alexa_done) {
//        this.alexa_done = alexa_done;
//    }
//
//    public Date getExport_date() {
//        return export_date;
//    }
//
//    public void setExport_date(Date export_date) {
//        this.export_date = export_date;
//    }
//
//    public String getSagepay() {
//        return sagepay;
//    }
//
//    public void setSagepay(String sagepay) {
//        this.sagepay = sagepay;
//    }

}
