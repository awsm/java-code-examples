import org.apache.log4j.Logger;

/**
 * ASCII messages FTW!
 */
public class ASCII {

    final static Logger logger = Logger.getLogger(ASCII.class);

    public static void startup() {
        logger.info("  _____ _             _               _ ");
        logger.info(" / ____| |           | |             | |");
        logger.info("| (___ | |_ __ _ _ __| |_ _   _ _ __ | |");
        logger.info(" \\___ \\| __/ _` | '__| __| | | | '_ \\| |");
        logger.info(" ____) | || (_| | |  | |_| |_| | |_) |_|");
        logger.info("|_____/ \\__\\__,_|_|   \\__|\\__,_| .__/(_)");
        logger.info("                               | |      ");
        logger.info("                               |_|      ");
    }


    public static void done() {
        logger.info(" _____                   _ ");
        logger.info("|  __ \\                 | |");
        logger.info("| |  | | ___  _ __   ___| |");
        logger.info("| |  | |/ _ \\| '_ \\ / _ \\ |");
        logger.info("| |__| | (_) | | | |  __/_|");
        logger.info("|_____/ \\___/|_| |_|\\___(_)");
    }

}
