import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used for making a logged and meaningful exit.
 */
public final class Exit {

    private static final Logger logger = LoggerFactory.getLogger(Exit.class);

    public static void withInfo(final String message) {
        exit(message, "INFO");
    }
    public static void withWarning(final String message) {
        exit(message, "WARNING");
    }
    public static void withError(final String message) {
        exit(message, "ERROR");
    }


    private static void exit(final String message, final String typeOfLogging) {

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                waitInMs(5000);
                log(message, typeOfLogging);
            }
        }));

        System.exit(0);
    }

    private static void log(final String message, final String typeOfLogging) {
        switch (typeOfLogging) {
            case "INFO": logger.info(message); break;
            case "WARNING": logger.warn(message); break;
            case "ERROR": logger.error(message); break;
        }
    }

    private static void waitInMs(final int wait) {
        try {
            Thread.sleep(wait);
        } catch (InterruptedException e) {
            e.printStackTrace(); // TODO
        }
    }

}
